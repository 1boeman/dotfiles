set expandtab
set tabstop=4
set shiftwidth=4
set autochdir
set number
set list
syntax on
set dir=~/.vimswap//,/var/tmp//,/tmp//,.

